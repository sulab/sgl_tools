import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.json.JSONObject;

public class ConfigContainer {
	public void initialize() {
		LoggingContainer.LogEvent("Reading property settings" );
		String path = System.getProperty( "user.dir" ) + System.getProperty( "file.separator" ) + "props.json";
		jsonConfig = null;
		try { 
			String content = readFile( path );
			if( content != null && content.length() > 0 ) {
				jsonConfig = new JSONObject( content );
			}
		}
	    catch( FileNotFoundException e ) {
			LoggingContainer.LogEvent("No props.json in install, using defaults" );			
	    }
		catch( Exception e ) {
			LoggingContainer.LogEvent("Property file exception " + e.toString() );			
		}
	}
	
	static JSONObject jsonConfig; 

	public static String defaultProperty( String name ) {
		switch( name ) {
		case "db":
			return "sciencedb";
		case "mixpercent":
			return "100";
		case "mixtoken":
			// this should be a default value for mixpanel
			return "5ba28fb2038b41de73b38774b7470ac3";
		case "mailerphp":
			return "/sections/sntmlr.php";
		case "site":
			return "www.sciencegamelab.org";
		case "SSL":
			return "0";
		case "transactions":
			return "0";
		case "scorecount":
			return "16";
		case "levelversion":
			return "v001";
		case "passwordhash":
			return "1234567890qwertyuiopasdfghjklzxcvbnmzaqxswcdevfrbgtnhymjukilop0987654321";
		}
		return "default";
	}
	public static String getProperty( String name ) {
		if( jsonConfig != null ) {
			if( jsonConfig.has( name ) ) {
				return jsonConfig.getString( name );
			}
		}
		return defaultProperty( name );
	}
	
	static String readFile(String fileName) throws IOException {
	    BufferedReader br = new BufferedReader(new FileReader(fileName));
	    try {
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();

	        while (line != null) {
	            sb.append(line);
	            sb.append("\n");
	            line = br.readLine();
	        }
	        return sb.toString();
	    } 
    	finally {
	        br.close();
	    }
	}
}
