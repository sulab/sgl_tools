import java.util.logging.Logger;

public class dbapp {

	public static MongoContainer mongo;
	public static LoggingContainer logging;
	public static ConfigContainer config;
	
	public static void main( String[] args ) {
		init();
		LoggingContainer.LogEvent( "Executing dbapp");
		
		if( args.length > 0 ) {
			processCommand( args );
		}
	}
	
	public static void processCommand( String[] args ) {
		switch( args[0].toLowerCase() ) {
			case "splitactions":
				LoggingContainer.LogEvent( "splitting actions" );
				splitActionsFromGames();
				break;
		}
	}
	
	public static void splitActionsFromGames() {
		mongo.splitActionsFromGames();
	}
	
	public static void init() {
		ConfigContainer config = new ConfigContainer();
		config.initialize();
		
		logging = new LoggingContainer();
		Logger logger = Logger.getLogger(dbapp.class.toString());
		LoggingContainer.initialize( logger );
		
		mongo = new MongoContainer();
		mongo.initialize();
	}
}
