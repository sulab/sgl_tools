import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;

import org.json.*;

public class MongoContainer {

	private boolean mConnected;
	private DB mDB;
	private MongoClient mMongo;

	public MongoContainer() {
	}

	private boolean checkConnected() {
		return mConnected;
	}

	public void initialize()  {
		LoggingContainer.LogEvent("Initializing Mongo");
	    try {
			mMongo = new MongoClient("localhost", 27017);
			mDB = mMongo.getDB(ConfigContainer.getProperty("db"));
			mConnected = true;
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (MongoException e) {
			e.printStackTrace();
		}
	}

	public boolean accountExists( String accountId ) {
		if( checkConnected() ) {
			DBCollection coll = mDB.getCollection("accounts");
			BasicDBObject searchQuery = new BasicDBObject();
			try {
				searchQuery.put("accountId", accountId );
				DBObject result = coll.findOne(searchQuery);
				if( result != null ) {
					return true;
				}
				return false;
			}
			catch( Exception e ) {
				return false;
			}
		}
		return false;
	}

	public JSONObject findGameAction( String gameid, String name ) {
		DBCollection coll = mDB.getCollection("actionconfig");
		JSONObject action = new JSONObject();
		BasicDBObject searchQuery = new BasicDBObject();
		List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
		obj.add(new BasicDBObject("gameid", gameid));
		obj.add(new BasicDBObject("name", name));
		searchQuery.put("$and", obj);
		DBObject result = coll.findOne(searchQuery);
		if( result != null ) {
			Iterator<String> entries = result.keySet().iterator();
			while( entries.hasNext() ) {
				String entry = entries.next().toString();
				action.put( entry, result.get( entry ) );
			}			
		}
		else {
			LoggingContainer.LogEvent( "not found " + gameid + ", name " + name );
		}
		return action;
	}
	
	public String createGameAction( JSONObject activity ) {
		String result = "";
		DBCollection coll = mDB.getCollection("actionconfig");
		BasicDBObject document = new BasicDBObject();
		Iterator<String> entries = activity.keySet().iterator();
		while( entries.hasNext() ) {
			String entry = entries.next().toString();
			document.put( entry, activity.get( entry ) );
		}
		coll.insert( document );
		JSONObject gameActivity = findGameAction( activity.getString( "gameid"), activity.getString( "name") );
		if( gameActivity.has( "_id" ) ) {
			result = gameActivity.get("_id").toString();
		}
		return result;
	}
	
	public void splitActionsFromGames() {
		DBCollection coll = mDB.getCollection("gameconfig");
		DBCursor cursor = coll.find();
		while(cursor.hasNext()) {
			DBObject result = cursor.next();
		    LoggingContainer.LogEvent( "found entry " + result.toString() );
			String value = result.get( "actions" ).toString();
			JSONArray activities = new JSONArray( value );
			JSONArray actionids = new JSONArray();
			for( int i = 0; i < activities.length(); i ++ ) {
				JSONObject activity = activities.getJSONObject(i);
				activity.put( "gameid",  result.get("_id").toString() );
			    LoggingContainer.LogEvent( "found action " + activity.toString() );
			    String myAction = createGameAction( activity );
			    LoggingContainer.LogEvent( "created action " + myAction );
			    actionids.put( myAction );
			}
			result.removeField( "actions" );
			result.put( "actionids", actionids.toString() );
			coll.save( result );
		    LoggingContainer.LogEvent( "created actionids " + actionids.toString() );
		}
	}
	
	public void testMongo()
	{
		LoggingContainer.LogEvent("Mongo Test Underway");
		try {
 
		/**** Connect to MongoDB ****/
		// Since 2.10.0, uses MongoClient
		MongoClient mongo = new MongoClient("localhost", 27017);
	 
		/**** Get database ****/
		// if database doesn't exists, MongoDB will create it for you
		DB db = mongo.getDB("testdb");
	 
		/**** Get collection / table from 'testdb' ****/
		// if collection doesn't exists, MongoDB will create it for you
		DBCollection table = db.getCollection("user");
	 
		/**** Insert ****/
		// create a document to store key and value
		BasicDBObject document = new BasicDBObject();
		document.put("name", "mkyong");
		document.put("age", 30);
		document.put("createdDate", new Date());
		table.insert(document);
	 
		/**** Find and display ****/
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("name", "mkyong");
	 
		DBCursor cursor = table.find(searchQuery);
	 
		while (cursor.hasNext()) {
			LoggingContainer.LogEvent(cursor.next().toString());
		}
	 
		/**** Update ****/
		// search document where name="mkyong" and update it with new values
		BasicDBObject query = new BasicDBObject();
		query.put("name", "mkyong");
	 
		BasicDBObject newDocument = new BasicDBObject();
		newDocument.put("name", "mkyong-updated");
	 
		BasicDBObject updateObj = new BasicDBObject();
		updateObj.put("$set", newDocument);
	 
		table.update(query, updateObj);
	 
		/**** Find and display ****/
		BasicDBObject searchQuery2 
		    = new BasicDBObject().append("name", "mkyong-updated");
	 
		DBCursor cursor2 = table.find(searchQuery2);
	 
		while (cursor2.hasNext()) {
			LoggingContainer.LogEvent(cursor2.next().toString());
		}
	 
		/**** Done ****/
		LoggingContainer.LogEvent("Done");
	 
	    } catch (UnknownHostException e) {
			e.printStackTrace();
	    } catch (MongoException e) {
			e.printStackTrace();
	    }
	}
}
