import org.json.JSONArray;
import org.json.JSONObject;

public class ParamsContainer {
	static String[] myPrivs = {
			"userConfig", "user",
			"userPassword", "user",
			"userEmail", "user",
			"gameConfig", "dev",
			"devConfig", "dev",
			"siteConfig", "admin",
			"userStatus", "admin",
	};

	static String[][] myInfoFields = {
			{ "user" }, { "accountAlias", "city", "country", "bio", "icon", "level", "points", "accountdob", "accountnewsletter" }
	};
	
	static String[][] mySettingFields = {
			{ "userConfig" }, { "accountAlias", "city", "country", "bio", "icon", "accountdob", "accountnewsletter" },
			{ "userPassword" }, { "oldpassword", "newpassword" },
			{ "userEmail" }, { "oldemail", "newemail" },
			{ "gameConfig" }, { "name", "icon", "index", "tags", "shortdescription", "longdescription", "url", "screenshot", "banner", "actions", "leaderboard" },
			{ "devConfig" }, { "devsecret" },
			{ "siteConfig" }, { "maxlevels", "levelcurve", "points", "actions" },
			{ "userStatus" }, { "status", "usertoken" },
	};
		
	static String[] myDefaultPoints = {
			"easy", "5",
			"medium", "15",
			"hard", "30",
			"extreme", "60",
			"social", "1",
			"gameplay", "1"
	};
	
	static String[] myDefaultLevels = {	
			"0",
			"50",
			"110",
			"180",
			"260",
			"350",
			"450",
			"560",
			"680",
			"810",
			"950",
			"1100",
			"1260",
			"1430",
			"1610",
			"1800",
			"2000",
			"2210",
			"2430",
			"2660",
			"2900",
			"3150",
			"3410",		
			"3680",		
			"3960",		
			"4250",		
			"4550",		
			"4860",		
			"5180",		
			"5510",		
			"5860",		
			"6230",		
			"6620",		
			"7030",		
			"7460",		
			"7915",		
			"8395",		
			"8900",		
			"9430",		
			"9985",		
			"10570",		
			"11185",		
			"11830",		
			"12505",		
			"13210",		
			"13950",		
			"14725",		
			"15535",		
			"16380",		
			"17260"		
	};

	public static String[][] myDefaultTags = {
			{ "Biology", "topic", "Biology Games", "These are games about Biology", "icon000.png" },
			{ "Cellular Biology", "topic", "Cellular Biology Games", "These are games about Cellular Biology", "icon000.png" },
			{ "Chemistry", "topic", "Chemistry Games", "These are games about Chemistry", "icon000.png" },
			{ "Disease", "topic", "Disease Games", "These are games about Disease", "icon000.png" },
			{ "Ecology", "topic", "Ecology Games", "These are games about Ecology", "icon000.png" },
			{ "Genetics", "topic", "Genetics Games", "These are games about Genetics", "icon000.png" },
			{ "Medicine", "topic", "Medicine Games", "These are games about Medicine", "icon000.png" },
			{ "Physics", "topic", "Physics Games", "These are games about Physics", "icon000.png" },
			{ "Scientific Research", "topic", "Scientific Research Games", "These are games about Scientific Research", "icon000.png" },
			{ "Space", "topic", "Space Games", "These are games about Space", "icon000.png" },
			{ "PC", "platform", "PC Games", "Games for the Windows PC", "icon000.png" },
			{ "Mac", "platform", "Mac Games", "Games for the MAC", "icon000.png" },
			{ "Online","platform", "Online Games", "Games for the web", "icon000.png" },
			{ "iOS","platform", "iOS Games", "Games for iPhone or iPad", "icon000.png" },
			{ "Android","platform", "Android Games", "Games for Android", "icon000.png" }
	};
	
	public static String[] myTagFields = {
			"name", "type", "label", "description","icon"
	};

	public static String[] myActionFields = {
			"name", "type", "label", "description", "points", "difficulty", "weight", "icon"
	};
	
	public static String[] myGameFields = {
			"name", "gamestate", "shortdescription", "longdescription", "tags", "icon", "banner", "screenshot", "actions", "url", "leaderboard"   
	};
	
	static String[][] myDefaultActions = {
			{ "levelup", "action", "Level up!", "User has went up a level", "none", "easy", "30", "icon000.png" },
			{ "gamestart", "action", "Started a game", "User has started a game", "none", "gameplay", "20", "icon000.png" },
			{ "register", "achievement", "Name1", "has joined Science GameLab!", "easy", "easy", "30", "icon000.png" } 
	};

	static String[][] myGameDefaultActions = {
			{ "easy", "action", "Easy Action", "did an easy task.", "easy", "easy", "10", "icon000.png" },
			{ "medium", "action", "Medium Action", "did a moderate task.", "medium", "medium", "20", "icon001.png" },
			{ "hard", "achievement", "Hard Action", "did a hard task.", "hard", "hard", "30", "icon002.png" },
			{ "extreme", "achievement", "Extreme Action", "did an extremely hard task.", "extreme", "extreme", "35", "icon003.png" }
	};
	
	static String[] myLeaderboardStates = { 
			"none",
			"ascending",
			"descending"
	};
	
	static String[] myGameStates = { 
		"live",
		"sandbox",
		"stage",
		"pending",
		"deleted",
		"inactive",
		"banned"
	};
    
    static String[] myDefaultGame = {
			"name0", "stage", "Short description", "This is a long description.", null, "icon000.png", "banner000.png", "screen000.png", null, "", "none" 
	};
    
    public static boolean validateLeaderboardSettings( String settings ) {
		for( int i = 0; i < myLeaderboardStates.length; i ++ ) {
			if( myLeaderboardStates[i].equals( settings ) ) {
				return true;
			}
		}
		return false;
    }
    
    public static boolean validateGameState( String gameState ) {
		for( int i = 0; i < myGameStates.length; i ++ ) {
			if( myGameStates[i].equals( gameState ) ) {
				return true;
			}
		}
		return false;
    }
    
	public static String getPrivileges( String myConfigType ) {
		for( int i = 0; i < myPrivs.length; i += 2 ) {
			if( myPrivs[i].equals( myConfigType ) ) {
				return myPrivs[i+1];
			}
		}
		return null;
	}
	
	public static String[] getInfoFields( String myConfigType ) {
		for( int i = 0; i < myInfoFields.length; i += 2 ) {
			if( myInfoFields[i][0].equals( myConfigType ) ) {
				return myInfoFields[i+1];
			}
		}
		return null;
	}

	public static String[] getSettingFields( String myConfigType ) {
		for( int i = 0; i < mySettingFields.length; i += 2 ) {
			if( mySettingFields[i][0].equals( myConfigType ) ) {
				return mySettingFields[i+1];
			}
		}
		return null;
	}

	public static JSONArray getDefaultTags() {
		JSONArray replyData = new JSONArray();
		for( int i = 0; i < myDefaultTags.length; i ++ ) {
			JSONObject newTag = new JSONObject();
			for( int j = 0; j < myTagFields.length; j ++ ) {
				newTag.put( myTagFields[j], myDefaultTags[i][j] );
			}
			replyData.put( newTag);
		}
		return replyData;
	}
	
	public static JSONObject getDefaultGameSettings() {
		JSONObject replyData = new JSONObject(); 
		for( int i = 0; i < myGameFields.length; i ++ ) {
			if( myDefaultGame[i] != null ) {
				replyData.put( myGameFields[i], myDefaultGame[i] );
			}
			else {
				if( myGameFields[i].equals( "actions") ) {
					JSONArray myGameActions = new JSONArray();
					for( int j = 0; j < myGameDefaultActions.length; j ++  ) {
						JSONObject gameAction = new JSONObject();
						for( int k = 0; k < myActionFields.length; k ++ ) {
							gameAction.put( myActionFields[k], myGameDefaultActions[j][k] );
						}
						myGameActions.put( gameAction );
					}
					replyData.put( myGameFields[i], myGameActions );
				}
				else {
					replyData.put( myGameFields[i], new JSONArray() );
				}
			}
		}
		return replyData;
	}
	
	public static JSONObject getDefaultSiteSettings() {
		JSONObject replyData = new JSONObject(); 
		replyData.put( "maxlevels", "" + myDefaultLevels.length );
		JSONObject data = new JSONObject();
		for( int i = 0; i < myDefaultLevels.length; i ++ ) {
			data.put( ""+i, myDefaultLevels[i] );
		}
		replyData.put( "levelcurve", data );
		
		JSONArray points = new JSONArray();
		for( int i = 0; i < myDefaultPoints.length; i += 2 ) {
			JSONObject entry = new JSONObject();
			entry.put( "name", myDefaultPoints[i] );
			entry.put( "value", myDefaultPoints[i+1] );
			points.put( entry );
		}
		replyData.put( "points", points );
		
		JSONArray actions = new JSONArray();
		for( int i = 0; i < myDefaultActions.length; i ++ ) {
			JSONObject entry = new JSONObject();
			for( int j = 0; j < myActionFields.length; j ++ ) {
				entry.put( myActionFields[j], myDefaultActions[i][j]);
			}
			actions.put( entry );
		}
		replyData.put( "actions", actions );
		
		replyData.put( "tags", getDefaultTags() );

		return replyData;
	}
	
	public static String validateSiteSettings( JSONObject configData ) {
		int maxLevels = 0;
		JSONArray data;
		if( configData.has( "tags" ) ) {
			for( int i = 0; i < configData.getJSONArray( "tags" ).length(); i ++ ) {
				JSONObject myTag = configData.getJSONArray( "tags" ).getJSONObject(i);
				for( int j = 0; j < myTagFields.length; j ++ ) {
					if( myTag.has( myTagFields[j] ) == false ) {
						return "Tag " + i + " is missing " +  myTagFields[j] + ": " + myTag.toString();
					}
				}
			}
		}
		if( configData.has( "maxlevels" ) && configData.has( "levelcurve" ) ) {
			maxLevels = Integer.parseInt( configData.getString( "maxlevels") );
			if( maxLevels > 0 ) {
				JSONObject levels = configData.getJSONObject( "levelcurve" );
				int value = 0;
				for( int i = 0; i < maxLevels; i ++ ) {
					if( levels.has( "" + i ) ) {
						int nextValue = Integer.parseInt( levels.getString( "" + i ) );
						if( i > 0 ) {
							if( nextValue <= value ) {
								return "level " + i + " has value not monotonically increasing, expecting value more than " + value + " but have " + nextValue;
							}
						}
						value = nextValue; 
					}
					else {
						return "level " + i + " experience value is missing";
					}
				}
			}
			else {
				return "maxlevels cannot be 0";
			}
		}
		else {
			if( configData.has( "maxlevels" ) ) {
				return "missing levelcurve";
			}
			if( configData.has( "levelcurve" ) ) {
				return "missing maxlevels";
			}
			
		}
		if( configData.has( "points" ) ) {
			data = configData.getJSONArray( "points" );
			if( data.length() > 0 ) {
				// validate these
			}
		}
		if( configData.has( "actions" ) ) {
			data = configData.getJSONArray( "actions" );
			if( data.length() > 0 ) {
				for( int i = 0; i < data.length(); i ++ ) {
					JSONObject entry = data.getJSONObject(i);
					for( int j = 0; j < myActionFields.length; j ++ ) {
						if( entry.has( myActionFields[j] ) == false ) {
							return "item " + i + ":" + entry.toString() + " is missing field: " + myActionFields[j];
						}
					}
				}
			}
		}		
		return null;
	}
}
